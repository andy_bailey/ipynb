VENVBIN := virtualenv
#PYPIHOST := ec2-54-85-159-185.compute-1.amazonaws.com
#PYPIURL := -i http://root:predictive123@$(PYPIHOST):6543/pypi/
VENVDIR := venv
PYTHON := $(VENVDIR)/bin/python2.7
ACTIVATE := . $(VENVDIR)/bin/activate

BREW := /usr/local/bin/brew
VPYTHON := venv/bin/python
PYTHON := /usr/local/bin/python

BREWDEPS := \
	zeromq \
	freetype \
	libpng \
	qt \
	phantomjs \
	slimerjs \
	pyqt \
	python \
	casperjs

all: .installpackages.ts

brew_install.rb:
	curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install > $@

$(BREW): brew_install.rb
	test -f $@ || ruby brew_install.rb

$(PYTHON): $(BREW)
	$(BREW) install python

$(VPYTHON): $(PYTHON)
	$(VENVBIN) --no-site-packages --always-copy $(VENVDIR)

.pipupgrade.ts: $(VPYTHON)
	$(ACTIVATE) && \
	pip install pip --upgrade && \
	touch $@

.brewdeps.ts: $(foreach PKG, $(BREWDEPS), .brewinst.$(PKG).ts)

.brewinst.%.ts: $(BREW)
	brew ls --versions $* 2>&1 /dev/null || brew install $*
	touch $@

.installpackages.ts: .brewdeps.ts .pipupgrade.ts requirements.txt
	$(ACTIVATE) && \
	pip install -r requirements.txt --upgrade && \
	touch $@

test: .installpackages.ts
	$(ACTIVATE) && \
	iptest

run: .installpackages.ts
	$(ACTIVATE) && ipython notebook
